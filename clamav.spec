Name:           clamav
Summary:        End-user tools for the Clam Antivirus scanner
Version:        0.101.4
Release:        7
License:        GPLv2
URL:            https://www.clamav.net/
Source0:        https://www.clamav.net/downloads/production/clamav-%version.tar.gz
Source1:        clamd.sysconfig
Source2:        clamd.logrotate
Source3:        main-58.cvd
Source4:        daily-25550.cvd
Source5:        bytecode-330.cvd
Source7:        freshclam-sleep
Source8:        freshclam.sysconfig
Source9:        clamav-update.crond
Source10:       clamav-update.logrotate
Source11:       clamav-milter.upstart
Source12:       clamav-milter.systemd
Source13:       clamd.scan.upstart
Source14:       clamd@scan.service
Source15:       clamd@.service

Patch0001:      clamav-0.100.0-stats-deprecation.patch
Patch0002:      clamav-0.100.1-defaults_locations.patch
Patch0003:      clamav-0.99-private.patch
Patch0004:      clamav-0.100.0-umask.patch
Patch0005:      llvm-glibc.patch
Patch0006:      clamav-Fix-int64-overflow-check.patch

BuildRequires:  autoconf automake gettext-devel libtool libtool-ltdl-devel
BuildRequires:  gcc-c++ zlib-devel bzip2-devel gmp-devel curl-devel json-c-devel
BuildRequires:  ncurses-devel openssl-devel libxml2-devel pcre2-devel libmilter-devel
BuildRequires:  bc tcl groff graphviz ocaml nc systemd-devel sendmail-devel
Requires:       data(clamav)
Provides:       bundled(libmspack) = 0.5-0.1.alpha.modified_by_clamav

Provides:       %{name}-lib = %{version}-%{release}
Obsoletes:      %{name}-lib < %{version}-%{release}

%description

Clam AntiVirus (clamav) is an open source antivirus engine for detecting trojans,
viruses, malware & other malicious threats. The main purpose of this software is
the integration with mail servers (attachment scanning). The package provides a
flexible and scalable multi-threaded daemon, a command line scanner, and a tool
for automatic updating via Internet. The programs are based on a shared library
distributed with the Clam AntiVirus package, which you can use with your own software.
he virus database is based on the virus database from OpenAntiVirus, but contains
additional signatures and is KEPT UP TO DATE.


%package        devel
Summary:        Header files and libraries for the Clam Antivirus scanner
Requires:       %{name} = %{version}-%{release} %{name}-filesystem = %{version}-%{release} openssl-devel

%description devel
The clamav-devel package contains headerfiles and libraries
which are needed to build applications using clamav.


%package        help
Summary:        man info for clamav

%description    help
The clamav-help package contains man information for clamav.


%package        filesystem
Summary:        Filesystem structure for clamav
Conflicts:      %{name} < %{version}-%{release} %{name} > %{version}-%{release}
Requires(pre):  shadow-utils
BuildArch:      noarch

%description    filesystem
The clamav-filesystem package provides the filesystem structure and
contains the user-creation scripts required by clamav.


%package        data
Summary:        Virus signature data for the Clam Antivirus scanner
Requires:       %{name}-filesystem = %{version}-%{release}
Provides:       data(clamav) = full %{name}-db = %{version}-%{release}
Obsoletes:      %{name}-db < %{version}-%{release}
BuildArch:      noarch

%description data
The clamav-data package contains the virus-database needed by clamav.
This database should be updated regularly; Use this package when you
want a working (but perhaps outdated) virus scanner immediately after
package installation.


%package        update
Summary:        Auto-updater for the Clam Antivirus scanner data-files
Requires:       %{name}-filesystem = %{version}-%{release} crontabs cronie
Provides:       data(clamav) = empty %{name}-data-empty = %{version}-%{release}
Obsoletes:      %{name}-data-empty < %{version}-%{release}
Requires(post): %__chown %__chmod

%description update
The clamav-update package contains programs which can be used to update
the clamav anti-virus database automatically. It uses the freshclam(1)
utility for this task. Use this package when you go updating the virus
database regulary and do not want to download a >120MB sized rpm-package
with outdated virus definitions.


%package        -n clamd
Summary:        The Clam AntiVirus Daemon
Requires:       data(clamav) coreutils %{name}-filesystem = %{version}-%{release}
Requires:       %{name} = %{version}-%{release}
Requires(pre):  shadow-utils
Obsoletes:      %{name}-server-sysvinit  < %{version}-%{release}
Obsoletes:      %{name}-scanner-sysvinit < %{version}-%{release}
Obsoletes:      %{name}-scanner-upstart  < %{version}-%{release}
Provides:       %{name}-scanner-systemd = %{version}-%{release}
Obsoletes:      %{name}-scanner-systemd < %{version}-%{release}
Provides:       %{name}-server-systemd = %{version}-%{release}
Obsoletes:      %{name}-server-systemd < %{version}-%{release}

Provides:       %{name}-server = %{version}-%{release}  %{name}-server-sysv = %{version}-%{release}
Obsoletes:      %{name}-server < %{version}-%{release}  %{name}-server-sysv < %{version}-%{release}
Provides:       %{name}-scanner = %{version}-%{release} %{name}-scanner-upstart = %{version}-%{release}
Obsoletes:      %{name}-scanner < %{version}-%{release} %{name}-scanner-upstart < %{version}-%{release}
Provides:       %{name}-server-sysvinit = %{version}-%{release}
Obsoletes:      %{name}-server-sysvinit < %{version}-%{release}


%description -n clamd
The Clam AntiVirus Daemon. The clamd package contains a generic system
wide clamd service which is e.g. used by the clamav-milter package.


%package         milter
Summary:        Milter module for the clamav scanner
Requires:       %{name}-filesystem = %{version}-%{release}
Requires(post):  coreutils
Requires(pre):   shadow-utils

Obsoletes:      %{name}-milter-sysvinit < %{version}-%{release}
Obsoletes:      %{name}-milter-upstart  < %{version}-%{release}
Provides:       %{name}-milter-systemd  = %{version}-%{release}
Obsoletes:      %{name}-milter-systemd  < %{version}-%{release}

%description milter
The clamav-milter package contains files which are needed to run the clamav-milter.


%prep
%autosetup -n %{name}-%{version}%{?prerelease} -p1

install -d libclamunrar{,_iface}
touch libclamunrar/{Makefile.in,all,install}

sed -ri -e 's!^#?(LogFile ).*!#\1/var/log/clamd.<SERVICE>!g' \
    -e 's!^#?(LocalSocket ).*!#\1%{_rundir}/clamd.<SERVICE>/clamd.sock!g' \
    -e 's!^(#?PidFile ).*!\1%{_rundir}/clamd.<SERVICE>/clamd.pid!g' \
    -e 's!^#?(User ).*!\1<USER>!g' \
    -e 's!^#?(AllowSupplementaryGroups|LogSyslog).*!\1 yes!g' \
    -e 's! /usr/local/share/clamav,! %_var/lib/clamav,!g' etc/clamd.conf.sample

sed -ri -e 's!^Example!#Example!'  -e 's!^#?(UpdateLogFile )!#\1!g;' \
    -e 's!^#?(LogSyslog).*!\1 yes!g' -e 's!(DatabaseOwner *)clamav$!\1clamav!g' \
    etc/freshclam.conf.sample


%build
export LDFLAGS='%{?__global_ldflags} -Wl,--as-needed'
export have_cv_ipv6=yes

rm -rf libltdl autom4te.cache Makefile.in
autoreconf -i
%configure --enable-milter --disable-clamav --disable-static --disable-zlib-vcheck \
    --disable-unrar --enable-id-check --enable-dns --with-dbdir=%_var/lib/clamav \
    --with-group=clamav --with-user=clamav --disable-rpath \
    --disable-silent-rules --enable-clamdtop

sed -i -e 's! -shared ! -Wl,--as-needed\0!g' \
    -e '/sys_lib_dlsearch_path_spec=\"\/lib \/usr\/lib /s!\"\/lib \/usr\/lib !/\"/%_lib /usr/%_lib !g' \
    libtool

%make_build


%install
rm -rf _doc*
%make_install

function smartsubst() {
    local tmp
    local regexp=$1
    shift

    tmp=$(mktemp /tmp/%name-subst.XXXXXX)
    for i; do
        sed -e "$regexp" "$i" >$tmp
        cmp -s $tmp "$i" || cat $tmp >"$i"
        rm -f $tmp
    done
}

install -d -m 0755 $RPM_BUILD_ROOT%_sysconfdir/{mail,clamd.d,logrotate.d} \
    $RPM_BUILD_ROOT%_tmpfilesdir $RPM_BUILD_ROOT%_rundir $RPM_BUILD_ROOT%_var/log \
    $RPM_BUILD_ROOT%_rundir/clamav-milter $RPM_BUILD_ROOT%_datadir/%name/template \
    $RPM_BUILD_ROOT%_initrddir $RPM_BUILD_ROOT%_var/lib/clamav $RPM_BUILD_ROOT%_rundir/clamd.scan

%delete_la

touch $RPM_BUILD_ROOT%_var/lib/clamav/{daily,main,bytecode}.cld
touch $RPM_BUILD_ROOT%_var/lib/clamav/mirrors.dat

install -D -m 0644 -p %SOURCE3     $RPM_BUILD_ROOT%_var/lib/clamav/main.cvd
install -D -m 0644 -p %SOURCE4     $RPM_BUILD_ROOT%_var/lib/clamav/daily.cvd
install -D -m 0644 -p %SOURCE5     $RPM_BUILD_ROOT%_var/lib/clamav/bytecode.cvd
install -D -m 0644 -p %SOURCE1      _doc_server/clamd.sysconfig
install -D -m 0644 -p %SOURCE2      _doc_server/clamd.logrotate
install -D -m 0644 -p etc/clamd.conf.sample _doc_server/clamd.conf
install -D -p _doc_server/*            $RPM_BUILD_ROOT%_datadir/%name/template
install -D -p -m 0644 %SOURCE15        $RPM_BUILD_ROOT%_unitdir/clamd@.service
install -D -m 0644 -p %SOURCE10    $RPM_BUILD_ROOT%_sysconfdir/logrotate.d/clamav-update
touch $RPM_BUILD_ROOT%_var/log/freshclam.log
install -D -p -m 0755 %SOURCE7    $RPM_BUILD_ROOT%_datadir/%name/freshclam-sleep
install -D -p -m 0644 %SOURCE8    $RPM_BUILD_ROOT%_sysconfdir/sysconfig/freshclam
install -D -p -m 0600 %SOURCE9    $RPM_BUILD_ROOT%_sysconfdir/cron.d/clamav-update
mv -f $RPM_BUILD_ROOT%_sysconfdir/freshclam.conf{.sample,}
chmod 600 $RPM_BUILD_ROOT%_sysconfdir/freshclam.conf

smartsubst 's!webmaster,clamav!webmaster,clamav!g;
            s!/usr/share/clamav!%_datadir/%name!g;
            s!/usr/bin!%_bindir!g;
            s!/usr/sbin!%_sbindir!g;' \
   $RPM_BUILD_ROOT%_sysconfdir/cron.d/clamav-update \
   $RPM_BUILD_ROOT%_datadir/%name/freshclam-sleep

sed -e 's!<SERVICE>!scan!g;s!<USER>!clamscan!g' \
    etc/clamd.conf.sample > $RPM_BUILD_ROOT%_sysconfdir/clamd.d/scan.conf

install -D -p -m 0644 %SOURCE13 $RPM_BUILD_ROOT%_sysconfdir/init/clamd.scan.conf

cat << EOF > $RPM_BUILD_ROOT%_tmpfilesdir/clamd.scan.conf
d %_rundir/clamd.scan 0710 clamscan virusgroup
EOF

touch $RPM_BUILD_ROOT%_rundir/clamd.scan/clamd.{sock,pid}


sed -r -e 's!^#?(User).*!\1 clamilt!g' \
    -e 's!^#?(AllowSupplementaryGroups|LogSyslog) .*!\1 yes!g' \
    -e 's! /tmp/clamav-milter.socket! %_rundir/clamav-milter/clamav-milter.socket!g' \
    -e 's! /var/run/clamav-milter.pid! %_rundir/clamav-milter/clamav-milter.pid!g' \
    -e 's! /var/run/clamd/clamd.socket! %_rundir/clamd.scan/clamd.sock!g' \
    -e 's! /tmp/clamav-milter.log! %_var/log/clamav-milter.log!g' \
    etc/clamav-milter.conf.sample > $RPM_BUILD_ROOT%_sysconfdir/mail/clamav-milter.conf

install -D -p -m 0644 %SOURCE11 $RPM_BUILD_ROOT%_sysconfdir/init/clamav-milter.conf
install -D -p -m 0644 %SOURCE12 $RPM_BUILD_ROOT%_unitdir/clamav-milter.service

cat << EOF > $RPM_BUILD_ROOT%_tmpfilesdir/clamav-milter.conf
d %_rundir/clamav-milter 0710 clamilt clamilt
EOF

touch $RPM_BUILD_ROOT{%_rundir/clamav-milter/clamav-milter.{socket,pid},%_var/log/clamav-milter.log}


%check
make check


%pre filesystem
getent group  clamav >/dev/null || groupadd -r clamav
getent passwd clamav >/dev/null || \
       useradd -r -g clamav -d %_var/lib/clamav -s /sbin/nologin \
       -c "Clamav database update user" clamav
getent group virusgroup >/dev/null || groupadd -r virusgroup
usermod clamav -a -G virusgroup
exit 0


%pre -n clamd
getent group clamscan >/dev/null || groupadd -r clamscan
getent passwd clamscan >/dev/null || \
    useradd -r -g clamscan -d / -s /sbin/nologin \
    -c "Clamav scanner user" clamscan
usermod clamscan -a -G virusgroup
exit 0

%post -n clamd
[ -L /etc/systemd/system/multi-user.target.wants/clamd@scan.service ] &&
    ln -sf /usr/lib/systemd/system/clamd@.service /etc/systemd/system/multi-user.target.wants/clamd@scan.service  || :
%systemd_post clamd@scan.service
/bin/systemd-tmpfiles --create %_tmpfilesdir/clamd.scan.conf || :

%preun -n clamd
%systemd_preun clamd@scan.service

%postun -n clamd
%systemd_postun_with_restart clamd@scan.service

%post update
test -e %_var/log/freshclam.log || {
    touch %_var/log/freshclam.log
    %__chmod 0664 %_var/log/freshclam.log
    %__chown root:clamav %_var/log/freshclam.log
    ! test -x /sbin/restorecon || /sbin/restorecon %_var/log/freshclam.log
}

%triggerin milter -- clamav-scanner
/usr/sbin/groupmems -g clamscan -a clamilt &>/dev/null || :

%pre milter
getent group clamilt >/dev/null || groupadd -r clamilt
getent passwd clamilt >/dev/null || \
    useradd -r -g clamilt -d %_rundir/clamav-milter -s /sbin/nologin \
    -c "Clamav Milter user" clamilt
usermod clamilt -a -G virusgroup
exit 0

%post milter
test -e %_var/log/clamav-milter.log || {
    touch %_var/log/clamav-milter.log
    chmod 0620             %_var/log/clamav-milter.log
    chown root:clamilt %_var/log/clamav-milter.log
    ! test -x /sbin/restorecon || /sbin/restorecon %_var/log/clamav-milter.log
}
%systemd_post clamav-milter.service
/bin/systemd-tmpfiles --create %_tmpfilesdir/clamav-milter.conf || :

%preun milter
%systemd_preun clamav-milter.service

%postun milter
%systemd_postun_with_restart clamav-milter.service

%post
/sbin/ldconfig

%postun
/sbin/ldconfig


%files
%exclude %_unitdir/clamav-{daemon,freshclam}.*
%exclude %_rundir/*/*.pid
%doc NEWS.md README.md docs/html COPYING
%_bindir/{clambc,clamconf,clamdscan,clamdtop,clamscan,clamsubmit,sigtool}
%_libdir/libclamav.so.9*
%_libdir/libclammspack.so.0*

%files devel
%_includedir/*
%_libdir/*.so
%_datadir/%name/template
%_libdir/pkgconfig/*
%_bindir/clamav-config

%files help
%_mandir/man[15]/*
%_mandir/man8/clamd.8*
%_mandir/man8/clamav-milter*
%_mandir/*/freshclam*

%files filesystem
%attr(-,clamav,clamav) %dir %_var/lib/clamav
%attr(-,root,root)  %dir %_datadir/%name
%exclude %_sysconfdir/clamd.conf.sample
%exclude %_sysconfdir/clamav-milter.conf.sample
%exclude %_sysconfdir/init
%dir %_sysconfdir/clamd.d

%files data
%defattr(-,clamav,clamav,-)
%config %verify(not size md5 mtime) %_var/lib/clamav/*.cvd

%files update
%_bindir/freshclam
%_datadir/%name/freshclam-sleep
%config(noreplace) %verify(not mtime)    %_sysconfdir/freshclam.conf
%config(noreplace) %verify(not mtime)    %_sysconfdir/logrotate.d/*
%config(noreplace) %_sysconfdir/cron.d/clamav-update
%config(noreplace) %_sysconfdir/sysconfig/freshclam
%ghost %attr(0664,root,clamav) %verify(not size md5 mtime) %_var/log/freshclam.log
%ghost %attr(0664,clamav,clamav) %_var/lib/clamav/*.cld
%ghost %attr(0664,clamav,clamav) %_var/lib/clamav/mirrors.dat

%files -n clamd
%doc _doc_server/*
%_sbindir/clamd
%_unitdir/clamd@.service

%config(noreplace) %_sysconfdir/clamd.d/scan.conf
%ghost %_rundir/clamd.scan/clamd.sock
%_tmpfilesdir/clamd.scan.conf
%ghost %dir %attr(0710,clamscan,virusgroup) %_rundir/clamd.scan


%files milter
%_sbindir/*milter*
%dir %_sysconfdir/mail
%config(noreplace) %_sysconfdir/mail/clamav-milter.conf
%ghost %attr(0620,root,clamilt) %verify(not size md5 mtime) %_var/log/clamav-milter.log
%ghost %_rundir/clamav-milter/clamav-milter.socket
%_tmpfilesdir/clamav-milter.conf
%ghost %dir %attr(0710,clamilt,clamilt) %_rundir/clamav-milter
%_unitdir/clamav-milter.service


%changelog
* Fri Oct 09 2020 lingsheng <lingsheng@huawei.com> - 0.101.4-7
- Fix int64 overflow check

* Tue Sep 21 2020 chengzihan <chengzihan2@huawei.com> - 0.101.4-6
- Drop clamd@scann.service file, change /var/run to /run

* Thu Mar 12 2020 wutao <wutao61@huawei.com> - 0.101.4-5
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:change spec files

* Mon Jan 20 2020 fengbing <fengbing7@huawei.com> - 0.101.4-4
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:change spec files

* Fri Dec 13 2019 zoushuangshuang<zoushuangshuang@huawei.com> - 0.101.4-2
- Package init
